---
title: "Home"
date: 2020-01-01T12:55:44-05:00
draft: false
layout: home
---

{{< rawhtml >}}
    Listen to this page rather than read it:
    <br><br>
    <audio controls>
    <source src="audio/home.mpeg" type="audio/mpeg">
    Your browser does not support the audio element.
    </audio>
{{< /rawhtml >}}

Welcome to my Vescovo page, a cookie-free, privacy-respecting, personal site, of which every single file, folder, line, word and character of code has been typed by hand, on the exact same keyboard, by the exact same person.

It's a little project. But it's also a personal reaction to the state of affairs of the internet and economy in general.

Personally, it's time for me to do some web like it's 1989. In this current epoch of cut and pasting, of social media, data harvesting, tracking and AI, of mega corps, centralisation and political overreach, of auto-generated language, content and code, it's time to go back to the artisan, craft web.

It's time to put back the human in words that can circle the globe, reach everyone but also remain personal and local, lived and felt, thought and expressed. 

So, here it's a lot of HTML, a sprinkling of CSS, and that's about it. And it's all put together with Hugo, and kindly hosted by GitLab.

It doesn't depend on third-party themes, libraries or plugins, or thousands of lines of code that few will ever read, and many will never understand.

There's no cookie banner to click and no ads or analytics. I'm talking to the void, but also to the world.

Nor is this launched as a finished website, complete and almost perfect from day one, and then minimally iterated. It's a work in progress, and you'll be able to see the process, step by step as it goes.

There'll be rough edges, sudden changes of direction, things that appear and disappear, and that you'll only see if you come back and check.
