---
title: "About"
date: 2020-01-01T12:55:44-05:00
draft: false
layout: about
---

### About this page

{{< rawhtml >}}
    Listen to this section rather than read it:
    <br><br>
    <audio controls>
    <source src="audio/home.mpeg" type="audio/mpeg">
    Your browser does not support the audio element.
    </audio>
{{< /rawhtml >}}

Welcome to my Vescovo page, a cookie-free, privacy-respecting, personal site, of which every single file, folder, line, word and character of code has been typed by hand, on the exact same keyboard, by the exact same person.

It's a little project. But it's also a personal reaction to the state of affairs of the internet and economy in general.

Personally, it's time for me to do some web like it's 1989. In this current epoch of cut and pasting, of social media, data harvesting, tracking and AI, of mega corps, centralisation and political overreach, of auto-generated language, content and code, it's time to go back to the artisan, craft web.

It's time to put back the human in words that can circle the globe, reach everyone but also remain personal and local, lived and felt, thought and expressed. 

So, here it's a lot of HTML, a sprinkling of CSS, and that's about it. And it's all put together with Hugo, and kindly hosted by GitLab.

It doesn't depend on third-party themes, libraries or plugins, or thousands of lines of code that few will ever read, and many will never understand.

There's no cookie banner to click and no ads or analytics. I'm talking to the void, but also to the world.

Nor is this launched as a finished website, complete and almost perfect from day one, and then minimally iterated. It's a work in progress, and you'll be able to see the process, step by step as it goes.

There'll be rough edges, sudden changes of direction, things that appear and disappear, and that you'll only see if you come back and check.

### About me

Hi, I'm Adam Bishop. Hundreds on the web have the same name. I'm neither the strongman, nor the saxophonist.

Not having a unique name on the web means I have to invent one. "Vescovo" is Italian for "Bishop", and rarely used as a surname.

Hailing from Essex, I have lived in various European countries and done various jobs. I have a scientific background but also a passion for art, music, poetry, words and languages. 

Though I like to have many projects on the go, that's partly why my main professional activity today is as an Italian, French and Spanish to English freelance general and specialist translator.

I like trying to understand things, and trying to help others understand things. I like to make things simple for others and simple things complicated for myself.

I studied physics and meteorology, because I wanted to understand the universe, and the wonder and complexity of fluids.

Structure, but also flow. Reason, but also beauty. Symmetry, but also chaos. Science, but also art.

I studied the philosophy, psychology and culture of science, because I wanted to put complexity in simple words, for everyone to be able to experience things that go beyond their own immediacy. 

For theoretical reasons, I should have studied mathematics. For pragmatic reasons, I should have studied either engineering or computer studies. But I didn't.

I have always loved humans and robots, history and science fiction, tinkering with hardware and software, breaking things apart and fixing them up, studying their details.

I loved programming from the moment, at about the age of about ten, I set sight on a BBC Acorn Electron. I loved the sound of the tape going round, the anticipation of the system loading.

I loved typing in programmes from the back of a magazine, with lines that referenced each other with a simple "GOTO". And then adding my own adaptations. Inventing my own games.

In time, I discovered the world of free, libre and open source software, or "FLOSS". Software that you can peer into, see how it works, improve on, contribute to, for the benefit of everyone, or adapt to your own needs.

This is why I predominantly use Linux, and as much FLOSS and as little proprietary, opaque and closed source software and hardware as I can.

Since people ask me to, I sometimes create multilingual websites for them, and I use all sorts of tools to get the result that they want.

However, in this personal website, I wanted to go back to the simplicity and innocence of my youth.

I wanted my own personal website to rely on a minimal set of tools. Not impersonal tools, themes or libraries of others bootstrapped and plugged in.

Today, you can more or less press a button and get a website. Pay someone and get a website. Or rather ask AI to create one.

But that's not personal. That says little or nothing about you. In an age of manufactured appearances and manufactured contents, there's all the more need to be who you are, warts and all.

So here is my website, which somehow expresses, more or less, partly or wholly, my personality.

Feel free to reach out to me on the social media of your choice. Links are in the footer at the bottom of every page.
